import 'package:flutter/material.dart';
import 'package:sample_app/features/homepage/ui/hompeage_feature_widget.dart';
import 'package:sample_app/features/firstScreen/ui/firstScreen_feature_widget.dart';

class Routes {
  static const String initialRoute = '/';
  static const String secondPage = '/secondPage';

  // static const String payment = '/payment';
  static Widget generate(String routeName) {
    switch (routeName) {
      case initialRoute:
        return HomePageFeatureWidget();
      case secondPage:
        return FirstScreenFeatureWidget();
      default:
        return Scaffold(
          body: Center(
            child: Text('404, Page Not Found!'),
          ),
        );
    }
  }
}
