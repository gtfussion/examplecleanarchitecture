import 'package:clean_framework/clean_framework.dart';

class HomepageEntity extends Entity {
  final String lastHomepage;
  final int firstScreenCount;
  final String username;
  HomepageEntity(
      {List<EntityFailure> errors = const [],
      this.lastHomepage = '',
      this.firstScreenCount = 0,
      this.username = ''});

  @override
  List<Object> get props => [errors, lastHomepage, firstScreenCount, username];

  @override
  HomepageEntity merge(
      {errors, String userName, String password, String username}) {
    return HomepageEntity(
        errors: errors ?? this.errors,
        lastHomepage: lastHomepage ?? this.lastHomepage,
        firstScreenCount: firstScreenCount ?? this.firstScreenCount,
        username: username ?? this.username);
  }
}
