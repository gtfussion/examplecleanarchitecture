import 'package:clean_framework/clean_framework.dart';

class HomepageModel extends ViewModel {
  final String username;
  HomepageModel({this.username});

  @override
  List<Object> get props => [username];
}

enum ServiceStatus { success, fail, unknown }
enum DataStatus { valid, invalid, unknown }
