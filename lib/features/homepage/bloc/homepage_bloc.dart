import 'package:clean_framework/clean_framework.dart';
import 'package:sample_app/features/homepage/bloc/homepage_usecase.dart';
import 'package:sample_app/features/homepage/model/home_view_model.dart';

class HomepageBloc extends Bloc {
  HomepageUseCase _homepageUseCase;

  final homepageModelPipe = Pipe<HomepageModel>();

  HomepageBloc() {
    _homepageUseCase =
        HomepageUseCase((viewModel) => homepageModelPipe.send(viewModel));
    homepageModelPipe.whenListenedDo(_homepageUseCase.updateData);
  }

  @override
  void dispose() {
    homepageModelPipe.dispose();
  }
}
