import 'package:clean_framework/clean_framework.dart';
import 'package:clean_framework/clean_framework_defaults.dart';
import 'package:sample_app/features/homepage/bloc/homepage_service_adapter.dart';
import 'package:sample_app/features/homepage/model/home_view_model.dart';
import 'package:sample_app/features/homepage/model/homepage_entity.dart';
import 'package:sample_app/locator.dart';

class HomepageUseCase extends UseCase {
  Function(ViewModel) _viewModelCallBack;
  RepositoryScope _scope;

  HomepageUseCase(Function(ViewModel) viewModelCallBack)
      : assert(viewModelCallBack != null),
        _viewModelCallBack = viewModelCallBack;

  void updateData() async {
    _scope = ExampleLocator().repository.containsScope<HomepageEntity>();
    if (_scope == null) {
      final newHomepageEntity = HomepageEntity();
      _scope = ExampleLocator()
          .repository
          .create<HomepageEntity>(newHomepageEntity, _notifySubscribers);
    } else {
      _scope.subscription = _notifySubscribers;
    }
    await ExampleLocator()
        .repository
        .runServiceAdapter(_scope, HomepageServiceAdapter());
  }

  void _notifySubscribers(entity) {
    _viewModelCallBack(buildViewModel(entity));
  }

  HomepageModel buildViewModel(HomepageEntity entity) {
    return HomepageModel(
      username: entity.username,
    );
  }

  HomepageModel buildViewModelForLocalUpdate(HomepageEntity entity) {
    return HomepageModel(username: entity.username);
  }
}
