import 'package:clean_framework/clean_framework.dart';
import 'package:clean_framework/clean_framework_defaults.dart';
import 'package:sample_app/features/homepage/api/homepage_service.dart';
import 'package:sample_app/features/homepage/api/homepage_service_response_model.dart';
import 'package:sample_app/features/homepage/model/homepage_entity.dart';

class HomepageServiceAdapter extends ServiceAdapter<HomepageEntity,
    JsonRequestModel, HomepageServiceResponseModel, HomepageService> {
  HomepageServiceAdapter() : super(HomepageService());

  @override
  HomepageEntity createEntity(HomepageEntity initialEntity,
      HomepageServiceResponseModel responseModel) {
    return HomepageEntity(
        lastHomepage: responseModel.lastHomepage,
        username: initialEntity.username);
  }
}
