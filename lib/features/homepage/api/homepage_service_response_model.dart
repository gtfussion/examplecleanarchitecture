import 'package:clean_framework/clean_framework_defaults.dart';
import 'package:equatable/equatable.dart';

class HomepageServiceResponseModel extends Equatable
    implements JsonResponseModel {
  final String lastHomepage;
  final int homepageCount;
  final bool didSucceed;
  @override
  List<Object> get props => [lastHomepage, homepageCount, didSucceed];
  HomepageServiceResponseModel.fromJson(Map<String, dynamic> json)
      : lastHomepage = json['title'] ?? 'today',
        homepageCount = json['id'] ?? 0,
        didSucceed = true;
}
