import 'package:clean_framework/clean_framework.dart';
import 'package:clean_framework/clean_framework_defaults.dart';
import 'package:sample_app/features/homepage/api/homepage_service_response_model.dart';
import 'package:sample_app/locator.dart';

class HomepageService
    extends EitherService<JsonRequestModel, HomepageServiceResponseModel> {
  HomepageService()
      : super(
            method: RestMethod.get,
            restApi: ExampleLocator().api,
            path: 'posts/1');

  @override
  HomepageServiceResponseModel parseResponse(
      Map<String, dynamic> jsonResponse) {
    return HomepageServiceResponseModel.fromJson(jsonResponse);
  }
}
