import 'package:clean_framework/clean_framework.dart';
import 'package:flutter/material.dart';
import 'package:sample_app/features/homepage/bloc/homepage_bloc.dart';
import 'package:sample_app/features/homepage/model/home_view_model.dart';
import 'package:sample_app/features/homepage/ui/homepage.dart';
import 'package:sample_app/router.dart';

class HomepagePresenter
    extends Presenter<HomepageBloc, HomepageModel, Homepage> {
  @override
  Stream<HomepageModel> getViewModelStream(HomepageBloc bloc) {
    return bloc.homepageModelPipe.receive;
  }

  @override
  Homepage buildScreen(
      BuildContext context, HomepageBloc bloc, HomepageModel viewModel) {
    return Homepage(
      viewModel: viewModel,
      navigateToDetailPage: (id) => _navigateToDetailPage(context, id),
    );
  }

  void _navigateToDetailPage(BuildContext context, id) {
    CFRouterScope.of(context).push(Routes.secondPage, arguments: id);
    // Navigator.push(
    //     context,
    //     MaterialPageRoute(
    //         builder: (context) => FirstScreenFeatureWidget(id: id)));
    // CFRouterScope.of(context).push(Routes.secondPage);
  }
}
