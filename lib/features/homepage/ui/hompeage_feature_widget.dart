import 'package:clean_framework/clean_framework.dart';
import 'package:flutter/material.dart';
import 'package:sample_app/features/homepage/bloc/homepage_bloc.dart';
import 'package:sample_app/features/homepage/ui/homepage_presenter.dart';

class HomePageFeatureWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => HomepageBloc(),
      child: HomepagePresenter(),
    );
  }
}
