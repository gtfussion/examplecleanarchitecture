import 'package:clean_framework/clean_framework.dart';
import 'package:flutter/material.dart';
import 'package:sample_app/features/homepage/model/home_view_model.dart';

class Homepage extends Screen {
  final HomepageModel viewModel;
  final Function navigateToDetailPage;
  final List<Map<Object, dynamic>> userList = [
    {
      'userId': 1,
      'id': 1,
      'title':
          "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
      'body':
          "quia et suscipit suscipit recusandae consequuntur expedita et cum reprehenderit molestiae ut ut quas totam nostrum rerum est autem sunt rem eveniet architecto"
    },
    {
      'userId': 1,
      'id': 2,
      'title': "qui est esse",
      'body':
          "est rerum tempore vitae sequi sint nihil reprehenderit dolor beatae ea dolores neque fugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis qui aperiam non debitis possimus qui neque nisi nulla"
    },
    {
      'userId': 1,
      'id': 3,
      'title': "ea molestias quasi exercitationem repellat qui ipsa sit aut",
      'body':
          "et iusto sed quo iure voluptatem occaecati omnis eligendi aut ad voluptatem doloribus vel accusantium quis pariatur molestiae porro eius odio et labore et velit aut"
    },
    {
      'userId': 1,
      'id': 4,
      'title': "eum et est occaecati",
      'body':
          "ullam et saepe reiciendis voluptatem adipisci sit amet autem assumenda provident rerum culpa quis hic commodi nesciunt rem tenetur doloremque ipsam iure quis sunt voluptatem rerum illo velit"
    },
    {
      'userId': 1,
      'id': 5,
      'title': "nesciunt quas odio",
      'body':
          "repudiandae veniam quaerat sunt sed alias aut fugiat sit autem sed est voluptatem omnis possimus esse voluptatibus quis est aut tenetur dolor neque"
    },
    {
      'userId': 1,
      'id': 6,
      'title': "dolorem eum magni eos aperiam quia",
      'body':
          "ut aspernatur corporis harum nihil quis provident sequi mollitia nobis aliquid molestiae perspiciatis et ea nemo ab reprehenderit accusantium quas voluptate dolores velit et doloremque molestiae"
    }
  ];
  Homepage({this.viewModel, this.navigateToDetailPage});

  @override
  Widget build(BuildContext context) {
    // final media = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Home Page'),
        ),
        body: ListView(
          children: [
            ListView.builder(
                shrinkWrap: true,
                itemCount: userList.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 5.0, horizontal: 8),
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          border: Border.all(color: Colors.grey[500])),
                      child: InkWell(
                          onTap: () {
                            navigateToDetailPage(userList[index]['id']);
                          },
                          child:
                              ListTile(title: Text(userList[index]['title']))),
                    ),
                  );
                })
          ],
        ),
      ),
    );
  }
}
