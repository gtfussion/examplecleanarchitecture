import 'package:clean_framework/clean_framework.dart';

class FirstScreenEntity extends Entity {
  final String lastFirstScreen;
  final int firstScreenCount;
  final int id;
  FirstScreenEntity(
      {List<EntityFailure> errors = const [],
      this.lastFirstScreen = '',
      this.firstScreenCount = 0,
      this.id = 0});

  @override
  List<Object> get props => [errors, lastFirstScreen, firstScreenCount, id];

  @override
  FirstScreenEntity merge({errors, String userName, String password, int id}) {
    return FirstScreenEntity(
        errors: errors ?? this.errors,
        lastFirstScreen: lastFirstScreen ?? this.lastFirstScreen,
        firstScreenCount: firstScreenCount ?? this.firstScreenCount,
        id: id ?? this.id);
  }
}
