import 'package:clean_framework/clean_framework.dart';

class FirstScreenViewModel extends ViewModel {
  final String lastFirstScreen;
  final int firstScreenCount;
  final int id;
  FirstScreenViewModel({this.id, this.lastFirstScreen, this.firstScreenCount});

  @override
  List<Object> get props => [lastFirstScreen, firstScreenCount, id];
}

enum ServiceStatus { success, fail, unknown }
enum DataStatus { valid, invalid, unknown }
