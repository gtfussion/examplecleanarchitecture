import 'package:clean_framework/clean_framework.dart';
import 'package:flutter/material.dart';
import 'package:sample_app/features/firstScreen/model/firstScreen_view_model.dart';

class FirstScreenScreen extends Screen {
  final FirstScreenViewModel viewModel;
  final VoidCallback navigateToMakePayment;
  final Function navigateToHomepage;

  final Function onInputChange;
  FirstScreenScreen(
      {this.viewModel,
      this.navigateToHomepage,
      this.navigateToMakePayment,
      this.onInputChange});

  @override
  Widget build(BuildContext context) {
    final media = MediaQuery.of(context).size;

    // final id = context.routeArguments<int>();

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('FirstPage'),
        ),
        body: Center(
          child: Card(
            child: Column(
              children: <Widget>[
                ListTile(
                  title: Text('Last FirstScreen'),
                  subtitle: Text((viewModel.lastFirstScreen).toString()),
                ),
                ListTile(
                  title: Text('FirstScreen Count'),
                  subtitle: Text(viewModel.firstScreenCount.toString()),
                ),
                SizedBox(height: media.width * .03),
                // _textFormField(Key('usename_key'), 'Enter Username',
                //     onInputChange, TextInputType.text),
                // ListTile(
                //   title: Text('Input Change'),
                //   subtitle: Text(viewModel.username),
                // ),
                // ElevatedButton(
                //   child: Text('Next page >'),
                //   onPressed: () {
                //     navigateToHomepage();
                //   },
                // )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

Widget _textFormField(Key key, String hintText, Function onChangeTextField,
    TextInputType textInputType) {
  return TextFormField(
    key: key,
    keyboardType: textInputType,
    textInputAction: TextInputAction.next,
    decoration: InputDecoration(
      prefixIcon: Icon(Icons.person),
      filled: true,
      hintText: hintText,
      contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
      focusedBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.lightGreen, width: 2.0),
      ),
      enabledBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: Colors.transparent, width: 2.0)),
    ),
    onChanged: (value) {
      onChangeTextField(value);
    },
  );
}
