import 'package:clean_framework/clean_framework.dart';
import 'package:flutter/material.dart';
import 'package:sample_app/features/firstScreen/bloc/firstScreen_bloc.dart';
import 'package:sample_app/features/firstScreen/ui/firstScreen_presenter.dart';

class FirstScreenFeatureWidget extends StatelessWidget {
  final String id;
  FirstScreenFeatureWidget({this.id});
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => FirstScreenBloc(),
      child: FirstScreenPresenter(),
    );
  }
}
