import 'package:sample_app/features/firstScreen/bloc/firstScreen_bloc.dart';
import 'package:clean_framework/clean_framework.dart';
import 'package:flutter/material.dart';
import 'package:sample_app/features/firstScreen/model/firstScreen_view_model.dart';
import 'package:sample_app/features/firstScreen/ui/firstScreen_screen.dart';

class FirstScreenPresenter extends Presenter<FirstScreenBloc,
    FirstScreenViewModel, FirstScreenScreen> {
  @override
  Stream<FirstScreenViewModel> getViewModelStream(FirstScreenBloc bloc) {
    return bloc.firstScreenViewModelPipe.receive;
  }

  @override
  FirstScreenScreen buildScreen(BuildContext context, FirstScreenBloc bloc,
      FirstScreenViewModel viewModel) {
    final id = context.routeArguments<int>();
    bloc.updateId.send(id);
    return FirstScreenScreen(
      viewModel: viewModel,
      // onInputChange: (value) {
      //   _onChangeUserName(bloc, value);
      // },
    );
  }

  // void _onChangeUserName(FirstScreenBloc bloc, String username) {
  //   bloc.usernamePipe.send(username);
  // }
}
