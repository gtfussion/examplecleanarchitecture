import 'package:clean_framework/clean_framework.dart';
import 'package:sample_app/features/firstScreen/api/first_screen_service.dart';
import 'package:sample_app/features/firstScreen/api/first_screen_service_request_model.dart';
import 'package:sample_app/features/firstScreen/api/first_screen_service_response_model.dart';
import 'package:sample_app/features/firstScreen/model/firstScreen_entity.dart';

class FirstScreenServiceAdapter extends ServiceAdapter<
    FirstScreenEntity,
    FirstScreenServiceRequestModel,
    FirstScreenServiceResponseModel,
    FirstScreenService> {
  FirstScreenServiceAdapter() : super(FirstScreenService());

  @override
  FirstScreenServiceRequestModel createRequest(FirstScreenEntity entity) {
    return FirstScreenServiceRequestModel(id: entity.id);
  }

  @override
  FirstScreenEntity createEntity(FirstScreenEntity initialEntity,
      FirstScreenServiceResponseModel responseModel) {
    return FirstScreenEntity(
      firstScreenCount: responseModel.firstScreenCount,
      lastFirstScreen: responseModel.lastFirstScreen,
    );
  }
}
