import 'package:clean_framework/clean_framework.dart';
import 'package:clean_framework/clean_framework_defaults.dart';
import 'package:flutter/material.dart';
import 'package:sample_app/features/firstScreen/bloc/firstScreen_service_adapter.dart';
import 'package:sample_app/features/firstScreen/model/firstScreen_entity.dart';
import 'package:sample_app/features/firstScreen/model/firstScreen_view_model.dart';
import 'package:sample_app/locator.dart';

class FirstScreenUseCase extends UseCase {
  Function(ViewModel) _viewModelCallBack;
  RepositoryScope _scope;
  BuildContext context;
  FirstScreenUseCase(Function(ViewModel) viewModelCallBack)
      : assert(viewModelCallBack != null),
        _viewModelCallBack = viewModelCallBack;
  void create() async {
    _scope = ExampleLocator()
        .repository
        .create<FirstScreenEntity>(FirstScreenEntity(), _notifySubscribers);
    await ExampleLocator()
        .repository
        .runServiceAdapter(_scope, FirstScreenServiceAdapter());
  }

  void _notifySubscribers(entity) {
    _viewModelCallBack(buildViewModel(entity));
  }

  void updateid(int id) async {
    final entity = ExampleLocator().repository.get<FirstScreenEntity>(_scope);
    final updatedEntity = entity.merge(id: id);
    ExampleLocator()
        .repository
        .update<FirstScreenEntity>(_scope, updatedEntity);
    _viewModelCallBack(buildViewModelForLocalUpdate(updatedEntity));
    await ExampleLocator()
        .repository
        .runServiceAdapter(_scope, FirstScreenServiceAdapter());
  }

  FirstScreenViewModel buildViewModel(FirstScreenEntity entity) {
    return FirstScreenViewModel(
      lastFirstScreen: entity.lastFirstScreen,
      firstScreenCount: entity.firstScreenCount,
      id: entity.id,
    );
  }

  FirstScreenViewModel buildViewModelForServiceUpdate(
      FirstScreenEntity entity) {
    if (entity.hasErrors()) {
      return FirstScreenViewModel(
          lastFirstScreen: entity.lastFirstScreen,
          firstScreenCount: entity.firstScreenCount,
          id: entity.id);
    } else {
      return FirstScreenViewModel(
          lastFirstScreen: entity.lastFirstScreen,
          firstScreenCount: entity.firstScreenCount,
          id: entity.id);
    }
  }

  FirstScreenViewModel buildViewModelForLocalUpdate(FirstScreenEntity entity) {
    return FirstScreenViewModel(
        lastFirstScreen: entity.lastFirstScreen,
        firstScreenCount: entity.firstScreenCount,
        id: entity.id);
  }

  FirstScreenViewModel buildViewModelForLocalUpdateWithError(
      FirstScreenEntity entity) {
    return FirstScreenViewModel(
        lastFirstScreen: entity.lastFirstScreen,
        firstScreenCount: entity.firstScreenCount,
        id: entity.id);
  }
}
