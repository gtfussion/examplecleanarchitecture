import 'package:clean_framework/clean_framework.dart';
import 'package:sample_app/features/firstScreen/api/first_screen_service.dart';
import 'package:sample_app/features/firstScreen/bloc/firstScreen_usecase.dart';
import 'package:sample_app/features/firstScreen/model/firstScreen_view_model.dart';

class FirstScreenBloc extends Bloc {
  FirstScreenUseCase _firstScreenUseCase;

  final firstScreenViewModelPipe = Pipe<FirstScreenViewModel>();
  final updateId = Pipe<int>();

  FirstScreenBloc({FirstScreenService firstScreenService}) {
    _firstScreenUseCase = FirstScreenUseCase(
        (viewModel) => firstScreenViewModelPipe.send(viewModel));
    firstScreenViewModelPipe.whenListenedDo(_firstScreenUseCase.create);
    updateId.receive.listen(_updateId);
  }
  void _updateId(int id) {
    _firstScreenUseCase.updateid(id);
  }

  @override
  void dispose() {
    firstScreenViewModelPipe.dispose();
    // submitPipe.dispose();
  }
}
