import 'package:clean_framework/clean_framework_defaults.dart';
import 'package:equatable/equatable.dart';

class FirstScreenServiceRequestModel extends Equatable
    implements JsonRequestModel {
  final int id;

  FirstScreenServiceRequestModel({this.id});

  @override
  Map<String, dynamic> toJson() {
    return {'id': id};
  }

  @override
  List<Object> get props => [id];
}
