import 'package:clean_framework/clean_framework.dart';
import 'package:clean_framework/clean_framework_defaults.dart';
import 'package:sample_app/features/firstScreen/api/first_screen_service_request_model.dart';
import 'package:sample_app/features/firstScreen/api/first_screen_service_response_model.dart';
import 'package:sample_app/locator.dart';

class FirstScreenService extends EitherService<FirstScreenServiceRequestModel,
    FirstScreenServiceResponseModel> {
  FirstScreenService()
      : super(
            method: RestMethod.get,
            restApi: ExampleLocator().api,
            path: 'posts/2');

  @override
  FirstScreenServiceResponseModel parseResponse(
      Map<String, dynamic> jsonResponse) {
    return FirstScreenServiceResponseModel.fromJson(jsonResponse);
  }
}
