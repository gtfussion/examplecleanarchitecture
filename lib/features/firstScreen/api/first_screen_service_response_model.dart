import 'package:clean_framework/clean_framework_defaults.dart';
import 'package:equatable/equatable.dart';

class FirstScreenServiceResponseModel extends Equatable
    implements JsonResponseModel {
  final String lastFirstScreen;
  final int firstScreenCount;
  final bool didSucceed;
  @override
  List<Object> get props => [lastFirstScreen, firstScreenCount, didSucceed];
  FirstScreenServiceResponseModel.fromJson(Map<String, dynamic> json)
      : lastFirstScreen = json['title'] ?? 'today',
        firstScreenCount = json['id'] ?? 0,
        didSucceed = true;
}
