import 'package:flutter_test/flutter_test.dart';
import 'package:clean_framework/clean_framework.dart';
import 'package:sample_app/features/firstScreen/api/firstScreen_service.dart';
import 'package:sample_app/features/firstScreen/api/firstScreen_service_response_model.dart';

void main() {
  test('AccountDetailService success', () async {
    final service = FirstScreenService();
    final eitherResponse = await service.request();

    expect(eitherResponse.isRight, isTrue);
    expect(
      eitherResponse.fold((_) {}, (m) => m),
      FirstScreenServiceResponseModel.fromJson({
        'lastFirstScreen': 'today',
        'firstScreenCount': 0,
        'didSucceed': true
      }),
    );
  });
}
