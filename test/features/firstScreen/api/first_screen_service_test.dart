import 'package:flutter_test/flutter_test.dart';
import 'package:sample_app/features/firstScreen/api/first_screen_service.dart';
import 'package:sample_app/features/firstScreen/api/first_screen_service_response_model.dart';

void main() {
  test('AccountDetailService success', () async {
    final service = FirstScreenService();
    final eitherResponse = await service.request();
    expect(eitherResponse.isRight, isTrue);
    expect(
      eitherResponse.fold((_) {}, (m) => m),
      FirstScreenServiceResponseModel.fromJson({
        'lastFirstScreen': 'today',
        'firstScreenCount': 0,
        'didSucceed': true
      }),
    );
  });
}
