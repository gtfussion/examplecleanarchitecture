import 'package:flutter_test/flutter_test.dart';
import 'package:sample_app/features/firstScreen/model/firstScreen_view_model.dart';

void main() {
  test('FirstScreenViewModel initialize', () {
    final viewModel = FirstScreenViewModel(
      lastFirstScreen: "Mr.",
      firstScreenCount: 1,
      id: 0,
    );
    expect(viewModel.lastFirstScreen, "Mr.");
    expect(viewModel.firstScreenCount, 1);
    expect(viewModel.id, "A");
  });
  test('FirstScreenViewModel initialize with error', () {
    try {
      FirstScreenViewModel(firstScreenCount: null);
      expect(false, true);
    } catch (error) {
      expect(error != null, true);
    }
  });
}
