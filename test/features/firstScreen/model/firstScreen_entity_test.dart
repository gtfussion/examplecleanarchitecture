import 'package:flutter_test/flutter_test.dart';
import 'package:sample_app/features/firstScreen/model/firstScreen_entity.dart';

void main() {
  test('CustomerEntity merge with errors = null', () {
    final entity = FirstScreenEntity(
        firstScreenCount: 1, lastFirstScreen: "Joe", username: "A");
    entity.merge(errors: null);
    expect(entity.firstScreenCount, 1);
    expect(entity.lastFirstScreen, "Joe");
    expect(entity.username, "A");
  });
}
