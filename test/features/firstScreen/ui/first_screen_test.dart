import 'package:clean_framework/clean_framework.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:sample_app/features/firstScreen/bloc/firstScreen_bloc.dart';
import 'package:sample_app/features/firstScreen/ui/firstScreen_feature_widget.dart';
import 'package:sample_app/features/firstScreen/ui/firstScreen_presenter.dart';

void main() {
  testWidgets('FirstScreen Widget, with true bloc',
      (WidgetTester tester) async {
    final testWidget = MaterialApp(
        home: BlocProvider<FirstScreenBloc>(
      child: FirstScreenFeatureWidget(),
      create: (_) => FirstScreenBloc(),
    ));

    await tester.pumpWidget(testWidget);
    await tester.pump(Duration(milliseconds: 500));

    expect(find.byType(FirstScreenPresenter), findsOneWidget);
  });
}
