import 'package:flutter_test/flutter_test.dart';
import 'package:sample_app/features/firstScreen/api/first_screen_service_response_model.dart';
import 'package:sample_app/features/firstScreen/bloc/firstScreen_service_adapter.dart';
import 'package:sample_app/features/firstScreen/model/firstScreen_entity.dart';

void main() {
  test('Entity is created by service adapter', () {
    final entity = FirstScreenServiceAdapter().createEntity(
        FirstScreenEntity(id: 0),
        FirstScreenServiceResponseModel.fromJson({
          'firstScreenCount': 1,
          'lastFirstScreen': 'lorem ipsum',
        }));

    expect(
        entity,
        FirstScreenEntity(
            firstScreenCount: 1, lastFirstScreen: 'lorem ipsum', id: 0));
  });
}
