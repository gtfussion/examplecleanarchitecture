import 'package:flutter_test/flutter_test.dart';
import 'package:sample_app/features/firstScreen/bloc/firstScreen_bloc.dart';
import 'package:sample_app/features/firstScreen/model/firstScreen_view_model.dart';

void main() {
  test('FirstScreenBloc gets firstScreen view model', () {
    final bloc = FirstScreenBloc();

    bloc.firstScreenViewModelPipe.receive.listen(expectAsync1((model) {
      expect(model, isA<FirstScreenViewModel>());
      expect(model.lastFirstScreen, "delectus aut autem");
      expect(model.firstScreenCount, 1);
      expect(model.id, 0);
    }));
  });
}
