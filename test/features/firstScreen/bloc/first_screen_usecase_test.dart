import 'package:flutter_test/flutter_test.dart';
import 'package:sample_app/features/firstScreen/bloc/firstScreen_usecase.dart';
import 'package:sample_app/features/firstScreen/model/firstScreen_view_model.dart';

void main() {
  test('FirstScreenUseCase initialize with error', () {
    try {
      FirstScreenUseCase(null);
      expect(false, true);
    } catch (error) {
      expect(error != null, true);
    }
  });

  test('FirstScreenUseCase initialize and create', () {
    final usecase = FirstScreenUseCase((viewModel) {
      expect(viewModel, isA<FirstScreenViewModel>());
    });
    usecase.create();
  });

  test('FirstScreenUseCase initialize and recreate', () {
    final usecase = FirstScreenUseCase((viewModel) {
      expect(viewModel, isA<FirstScreenViewModel>());
    });
    usecase.create();
    usecase.create(); //use to trigger else in the create
  });
}
